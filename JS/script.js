const valueForCalc = {
    numberFirst: '',
    numberSecond: '',
    sign: '',
}

const calc = document.querySelector('.keys');
const tablo = document.querySelector('.tablo');
const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, '.'];
const signs = ['+', '-', '*', '/'];
let number = '';
let numInMemory;
let signInMemory;
let countMemory = true;
const m = document.querySelector('.m');

function calculate(numberFirst, numberSecond, sign) {
    let result;
    if (sign === '+') {
        result = +numberFirst + (+numberSecond);
        tablo.setAttribute('value', result);
    }
    if (sign === '-') {
        result = +numberFirst - (+numberSecond);
        tablo.setAttribute('value', result);
    }
    if (sign === '/') {
        result = +numberFirst / (+numberSecond);
        tablo.setAttribute('value', result);
    }
    if (sign === '*') {
        result = +numberFirst * (+numberSecond);
        tablo.setAttribute('value', result);
    }

}

calc.addEventListener('click', function (event) {

    if (numbers.includes(+event.target.value)) {
        number += event.target.value.toString();
        tablo.setAttribute('value', number);
    };
    if (signs.includes(event.target.value)) {
        if (valueForCalc.numberFirst != '' && valueForCalc.sign != '') {
            valueForCalc.numberSecond = number;
            number = '';
            tablo.setAttribute('value', number);
            calculate(valueForCalc.numberFirst, valueForCalc.numberSecond, valueForCalc.sign);
            valueForCalc.numberFirst = '';
            valueForCalc.numberSecond = '';
            valueForCalc.sign = '';
        }
        valueForCalc.numberFirst = number;
        valueForCalc.sign = event.target.value;
        number = '';
    };
    if (event.target.value === '=') {
        valueForCalc.numberSecond = number;
        number = '';
        tablo.setAttribute('value', number);
        calculate(valueForCalc.numberFirst, valueForCalc.numberSecond, valueForCalc.sign);
        valueForCalc.numberFirst = '';
        valueForCalc.numberSecond = '';
        valueForCalc.sign = '';
    };
    if (event.target.value === 'm-' || event.target.value === 'm+') {
        m.style.display = 'block';
        number = tablo.value;
        numInMemory = number;
        if (event.target.value === 'm-') {
            signInMemory = '-';
        } else {
            signInMemory = '+';
        }
        number = '';
        tablo.setAttribute('value', number);
    };
    if (event.target.value === 'mrc') {
        if (countMemory) {
            valueForCalc.numberFirst = tablo.value;
            valueForCalc.numberSecond = numInMemory;
            valueForCalc.sign = signInMemory;
            countMemory = false;
            calculate(valueForCalc.numberFirst, valueForCalc.numberSecond, valueForCalc.sign);
        } else {
            m.style.display = 'none';
            numInMemory = '';
            signInMemory = '';
            valueForCalc.numberFirst = '';
            valueForCalc.numberSecond = '';
            valueForCalc.sign = '';
            number = '';
            countMemory = true;
            tablo.setAttribute('value', number);
            console.log(valueForCalc)
        }

    };
    if (event.target.value === 'C') {
        valueForCalc.numberFirst = '';
            valueForCalc.numberSecond = '';
            valueForCalc.sign = '';
            number = '';
            tablo.setAttribute('value', number);
    };
});